# ESP32 Cliente Web Socket

Se configuran los datos de conexión a la red: SECRET_SSID, SECRET_PASS y los datos de conexión al servidor de sockets: SECRET_WS_HOST, SECRET_WS_PORT
en el archivo "./arduino_secrets.h"

## Tarjeta de Desarrollo ESP32 Wifi + Bluetooth 4.2 Ble Nodemcu
Se conecta a un servidor de web sockets para enviar continuamente
el estado del relevador y las variables de temperatura detectadas en el ambiente.

### Especificaciones Técnicas
#### CPU and Memory
- Xtensa® Dual-Core 32-bit LX6 microprocessors, up to 600 MIPS
- 448 KByte ROM
- 520 KByte SRAM
- 16 KByte SRAM in RTC
- QSPI Flash/SRAM, up to 4 x 16 MBytes
- Power supply: 2.2 V to 3.6 V
#### Clocks and Timers
- Internal 8 MHz oscillator with calibration
- Internal RC oscillator with calibration
- External 2 MHz to 40 MHz crystal oscillator
- External 32 kHz crystal oscillator for RTC with calibration
- Two timer groups, including 2 x 64-bit timers and 1 x main watchdog in each group
- RTC timer with sub-second accuracy
- RTC watchdog
#### Advanced Peripheral Interfaces
- 12-bit SAR ADC up to 18 channels
- 2 × 8-bit D/A converters
- 10 × touch sensors
- Temperature sensor
- 4 × SPI
- 2 × I2S
- 2 × I2C
- 3 × UART
- 1 host (SD/eMMC/SDIO)
- 1 slave (SDIO/SPI)
- Ethernet MAC interface with dedicated DMA and IEEE 1588 support
- CAN 2.0
- IR (TX/RX)
- Motor PWM
- LED PWM up to 16 channels
- Hall sensor
- Ultra low power analog pre-amplifier
#### Development Support
- SDK Firmware for fast on-line programming
- Open source toolchains based on GCC

### Características
- Dimensiones: 51 x 27mm
- Rango de temperatura de operación: -40°C a +85°C

[Documentación ESP32](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/hw-reference/esp32/get-started-devkitc.html)

## Sensor de temperatura Ds18b20 a prueba de agua

### Especificaciones Técnicas
- Voltaje de Operación: 3 ~ 5.5 V
- Rango de Trabajo: - 55 ~ 125 C 
- Precisión en el rango de - 10 ~ 85 °C
- Resolución seleccionable de 9 ~ 12 bits
- Cubierta de Acero Inoxidable de alta calidad, previene la humedad y la oxidación
- A prueba de Agua
- No necesita componentes adicionales
- Cables: Rojo (+VCC). Blanco (DATA). Negro (GND)
- Protocolo 1 cable, solo necesita 1 pin para comunicarse
- Identificación única de 64 bits

### Características
- Dimensiones (sensor): 6 x 30 mm 
- Longitud de cable: 1 m


[DS18b20 Data-sheet](https://datasheets.maximintegrated.com/en/ds/DS18B20.pdf)

## Módulo relevador SRD-05VDC-SL-C
Relevador 5 volts SRD-05VDC-SL-C ampliamente utilizado en electrónica para proveer aislamiento y conmutar cargas de corriente alterna de hasta 10 Amperes.

La bobina de este relevador funciona con 5 volts, por lo que es adecuado si se desea alimentar desde la misma fuente que un Arduino o un micro-controlador PIC que funciona con 5 volts.


### Especificaciones Técnicas
- Corriente máxima: 10A 250VAC
- Resistencia de contacto: < = 100 m ( ohmios )
- Vida útil eléctrica: 100.000
- Vida mecánica: 10,000,000
- Voltaje de operación de la bobina: 5 VDC
- potencia de la bobina: 0.36W
- Aislamiento entre bobina y contactos: 1500V / min.
- Aislamiento contacto - contacto: 1000 VCA / min.

[SRD-05VDC-SL-C Data-sheet](http://www.datasheet.es/download.php?id=720556)