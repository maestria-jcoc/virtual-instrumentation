#include <ArduinoHttpClient.h>
#include <WiFi.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <ArduinoJson.h>
#include "arduino_secrets.h"

const char* ssid = SECRET_SSID;
const char* password = SECRET_PASS;
const char* sender_id = "ESP32_1";

const uint16_t port = SECRET_WS_PORT;
const char* host = SECRET_WS_HOST;

const int pinRelay = 2;
const int pinDatosTemp = 4;

IPAddress local_IP(192, 168, 100, 250);
IPAddress gateway(192, 168, 100, 1);
IPAddress subnet(255, 255, 255, 0);
IPAddress primaryDNS(8, 8, 8, 8);   //optional
IPAddress secondaryDNS(8, 8, 4, 4); //optional

WiFiClient wifi;
WebSocketClient client = WebSocketClient(wifi, host, port);

OneWire oneWire(pinDatosTemp);
DallasTemperature tempSensor(&oneWire);

void setup() {
  Serial.begin(115200);
  while (!Serial) {
    ; // Waiting for Serial to be ready
  }

  pinMode(pinRelay, OUTPUT); // Define el pin 2 como salida [LED integrado y Relay]
  tempSensor.begin(); // Inicializa el sensor de temperatura

  //WiFi.config(local_IP, gateway, subnet, primaryDNS, secondaryDNS);

  Serial.print("Attempting to connect to Network named: ");
  Serial.println(ssid);                   // print the network name (SSID);
  WiFi.begin(ssid, password);

  int counter = 0;
  while (WiFi.status() != WL_CONNECTED) {
    counter++;
    delay(250);
    Serial.print(".");
    if(counter >= 20) {
      Serial.println("No se pudo conectar al WiFi, reiniciando");
      ESP.restart();
    }
  }

  Serial.println();
  Serial.print("WiFi connected with IP: ");
  Serial.println(WiFi.localIP());

  Serial.println("Starting WebSocket client");
  client.begin();
}

void sendCurrentState() {
  StaticJsonDocument<256> docSend;

  tempSensor.requestTemperatures();
  float temperatureC = tempSensor.getTempCByIndex(0);
  float temperatureF = tempSensor.getTempFByIndex(0);

  if(temperatureC > 32 && digitalRead(pinRelay)) {
    Serial.println("Relevador apagado por seguridad! Temperatura de 32ºC sobrepasada");
    digitalWrite(pinRelay, 0);
    docSend["message"] = "Relevador apagado por seguridad";
  } else {
    docSend["message"] = "ESP32 Current State";
  }

  docSend["sender_id"] = sender_id;
  JsonObject pinStatus = docSend.createNestedObject("pinOutStatus");
  pinStatus["relay"] = digitalRead(pinRelay);
  pinStatus["tempC"] = temperatureC;
  pinStatus["tempF"] = temperatureF;

  Serial.print("Current state -> ");
  serializeJson(docSend, Serial);
  Serial.println();

  client.beginMessage(TYPE_TEXT);
  serializeJson(docSend, client);
  client.endMessage();
}

unsigned long previousMillis = 0; 
const long interval = 10000; // 10 segundos

void loop() {

  while (!client.connected()) {
    Serial.println("Socket disconnected, Restarting...");
    client.begin();
    delay(1000);
  }

  Serial.println("Connected");
  sendCurrentState();
  
  while (client.connected()) {

    /*unsigned long currentMillis = millis();

    if (currentMillis - previousMillis >= interval) {
      Serial.println("Sending ping");
      previousMillis = currentMillis;
      client.beginMessage(TYPE_PING);
      client.endMessage();
    }*/
    
    // check if a message is available to be received
    int messageSize = client.parseMessage();

    if (messageSize > 0) {
      StaticJsonDocument<256> docReceive;
      
      const String receivedJson = client.readString();
      //Serial.println(receivedJson);
      deserializeJson(docReceive, receivedJson);
      const String message = docReceive["message"];

      // Manejar el caso de recibir la señal para apagar el relevador
      Serial.print("Received a message: ");
      serializeJson(docReceive, Serial);
      Serial.println();

      if (message.equals("Toggle Relay")) {
        const uint8_t pin = docReceive["pin"];
        const uint8_t state = docReceive["state"];

        digitalWrite(pin, state);
      }
      
      sendCurrentState();
    }
  }
}
