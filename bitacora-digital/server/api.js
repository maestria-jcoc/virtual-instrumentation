const express = require('express')
const bodyParser = require('body-parser')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

const DB = require('./db')
const config = require('./config')

const router = express.Router()
const db = new DB("localhost", "root", "12345", "esp32_websocket")

router.use(bodyParser.urlencoded({extended: false}))
router.use(bodyParser.json())

router.post('/login', (req, res) => {
    db.selectByUser(req.body.user, (err, user) => {
        if (err) return res.status(500).send(` Error en el servidor. ${err}`)
        if (!user) return res.status(401).send('Usuario o Contraseña Incorrecto')
        let passwordIsValid = bcrypt.compareSync(req.body.pass, user.password);
        if (!passwordIsValid) return res.status(401).send('Usuario o Contraseña Incorrecto')
        let token = jwt.sign({id: user.id}, config["jwt-secret"], {
            expiresIn: 86400 // expires in 24 hours
        })
        delete user.password
        res.status(200).send({auth: true, token: token, user: user})
    })
})
router.get('/samples', (req, res) => {
    db.getSamples((err, samples) => {
        if (err) return res.status(500).send(` Error en el servidor. ${err}`)
        if (!samples) return res.status(200).send('No se encontraron resultados.')

        res.status(200).send(JSON.stringify(samples))
    })
})
router.post('/samples/create', (req, res) => {
    // Elimina los campos no requeridos
    delete req.body.sample.tecnicas_status

    // Convierte en JSON las técnicas
    req.body.sample.tecnicas = JSON.stringify(req.body.sample.tecnicas)
    const formValues = Object.values(req.body.sample)
    db.createSample(formValues, (err, sample) => {
        if (err) return res.status(500).send(` Error en el servidor. ${err}`)
        if (!sample) return res.status(500).send('Error saving the sample.')
        res.status(200).send({
            message: `Muestra guardada correctamente`,
            id: sample.insertId
        });
    })
})
router.get('/samples/:id', (req, res) => {
    db.getSampleById(req.params.id, (err, sample) => {
        if (err) return res.status(500).send(`Error en el servidor. ${err}`)
        if (!sample) return res.status(404).send(`No existe la muestra con id: ${req.params.id}`)
        res.status(200).send(sample)
    })
})
router.put('/samples/:id', (req, res) => {
    // Elimina los campos no requeridos
    delete req.body.sample.id
    delete req.body.sample.created_at

    // Convierte en JSON las técnicas
    req.body.sample.tecnicas = JSON.stringify(req.body.sample.tecnicas)

    const formValues = Object.values(req.body.sample)
    db.updateSampleById(req.params.id, formValues, (err, sample) => {
        if (err) return res.status(500).send(`Error en el servidor. ${err}`)
        if (!sample) return res.status(404).send(`No existe la muestra con id: ${req.params.id}`)
        res.status(200).send('Muestra actualizada correctamente')
    })
})
router.delete('/samples/:id', (req, res) => {
    db.deleteSample(req.params.id, (err, sample) => {
        if (err) return res.status(500).send(` Error en el servidor. ${err}`)
        if (!sample) return res.status(404).send(`No existe la muestra con id: ${req.params.id}`)
        res.status(200).send('Eliminado correctamente')
    })
})
router.get('/logs', (req, res) => {
    db.getLogsByDateRange(req.query, (err, logEntries) => {
        if (err) return res.status(500).send(` Error en el servidor. ${err}`)
        if (!logEntries) return res.status(404).send(`No existen registros entre las fechas seleccionadas`)
        res.status(200).send(logEntries)
    })
})

module.exports = router
