-- MariaDB dump 10.19  Distrib 10.5.9-MariaDB, for osx10.16 (x86_64)
--
-- Host: 127.0.0.1    Database: esp32_websocket
-- ------------------------------------------------------
-- Server version	10.5.9-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `relay_logs`
--

DROP TABLE IF EXISTS `relay_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relay_logs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `action` varchar(30) CHARACTER SET utf8 NOT NULL,
  `status` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`status`)),
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `controlador` varchar(100) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `status_logger_id_uindex` (`id`),
  KEY `status_logger_users_id_fk` (`user_id`),
  CONSTRAINT `status_logger_users_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=156 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relay_logs`
--

/*!40000 ALTER TABLE `relay_logs` DISABLE KEYS */;
INSERT INTO `relay_logs` (`id`, `user_id`, `action`, `status`, `timestamp`, `controlador`) VALUES (4,2,'Toggle Relay','0','2021-05-27 22:42:06','raspberry'),(5,2,'Toggle Relay','1','2021-05-27 22:42:10','arduino'),(6,2,'Toggle Relay','0','2021-05-27 22:42:21','arduino'),(7,2,'Toggle Relay','1','2021-05-27 22:42:27','raspberry'),(8,2,'Toggle Relay','0','2021-05-27 22:44:31','raspberry'),(9,2,'Toggle Relay','1','2021-05-27 22:44:34','raspberry'),(10,2,'Toggle Relay','1','2021-05-27 22:44:52','arduino'),(11,2,'Toggle Relay','0','2021-05-27 22:45:08','arduino'),(12,2,'Toggle Relay','0','2021-05-27 22:45:18','raspberry'),(13,2,'Toggle Relay','1','2021-05-27 22:45:27','arduino'),(14,2,'Toggle Relay','0','2021-05-27 22:46:56','arduino'),(15,2,'Toggle Relay','1','2021-05-27 22:46:58','raspberry'),(16,2,'Toggle Relay','0','2021-05-27 22:47:03','raspberry'),(17,2,'Toggle Relay','1','2021-05-27 22:47:08','arduino'),(18,2,'Toggle Relay','0','2021-05-27 22:47:10','arduino'),(19,2,'Toggle Relay','1','2021-05-27 22:47:13','raspberry'),(20,2,'Toggle Relay','1','2021-05-27 22:48:26','arduino'),(21,2,'Toggle Relay','0','2021-05-27 22:48:28','arduino'),(22,2,'Toggle Relay','0','2021-05-27 22:48:30','raspberry'),(23,2,'Toggle Relay','1','2021-05-27 22:48:32','raspberry'),(24,2,'Toggle Relay','1','2021-05-27 22:49:12','arduino'),(25,2,'Toggle Relay','0','2021-05-27 22:49:19','raspberry'),(26,2,'Toggle Relay','1','2021-05-27 22:49:33','raspberry'),(27,2,'Toggle Relay','0','2021-05-27 22:49:35','arduino'),(28,2,'Toggle Relay','1','2021-05-27 22:50:10','arduino'),(29,2,'Toggle Relay','0','2021-05-27 22:50:39','arduino'),(30,2,'Toggle Relay','0','2021-05-27 22:50:47','raspberry'),(31,2,'Toggle Relay','1','2021-05-27 22:51:13','arduino'),(32,2,'Toggle Relay','0','2021-05-27 22:51:23','arduino'),(33,2,'Toggle Relay','1','2021-05-27 22:51:28','raspberry'),(34,2,'Toggle Relay','1','2021-05-27 22:52:18','arduino'),(35,2,'Toggle Relay','1','2021-05-27 22:52:20','raspberry'),(36,2,'Toggle Relay','0','2021-05-27 22:52:23','raspberry'),(37,2,'Toggle Relay','0','2021-05-27 22:52:24','arduino'),(38,2,'Toggle Relay','1','2021-05-27 22:52:42','raspberry'),(39,2,'Toggle Relay','1','2021-05-27 22:52:44','arduino'),(40,2,'Toggle Relay','0','2021-05-27 22:52:48','arduino'),(41,2,'Toggle Relay','1','2021-05-27 22:52:49','arduino'),(42,2,'Toggle Relay','0','2021-05-27 22:52:50','raspberry'),(43,2,'Toggle Relay','1','2021-05-27 22:52:51','raspberry'),(44,2,'Toggle Relay','0','2021-05-27 22:52:54','arduino'),(45,2,'Toggle Relay','0','2021-05-27 22:52:55','raspberry'),(46,2,'Toggle Relay','1','2021-05-27 23:27:07','arduino'),(47,2,'Toggle Relay','1','2021-05-27 23:29:14','raspberry'),(48,2,'Toggle Relay','0','2021-05-27 23:29:14','raspberry'),(49,2,'Toggle Relay','0','2021-05-27 23:29:15','arduino'),(50,2,'Toggle Relay','1','2021-05-27 23:29:16','arduino'),(51,2,'Toggle Relay','0','2021-05-27 23:29:19','arduino'),(52,2,'Toggle Relay','1','2021-05-27 23:29:21','raspberry'),(53,2,'Toggle Relay','0','2021-05-27 23:29:22','raspberry'),(54,2,'Toggle Relay','1','2021-05-28 00:02:49','arduino'),(55,2,'Toggle Relay','0','2021-05-28 00:02:51','arduino'),(56,2,'Toggle Relay','1','2021-05-28 00:02:52','raspberry'),(57,2,'Toggle Relay','0','2021-05-28 00:02:53','raspberry'),(58,2,'Toggle Relay','1','2021-05-28 00:07:53','arduino'),(59,2,'Toggle Relay','1','2021-05-28 00:08:41','arduino'),(60,2,'Toggle Relay','1','2021-05-28 00:10:14','arduino'),(61,2,'Toggle Relay','1','2021-05-28 00:10:26','arduino'),(62,2,'Toggle Relay','1','2021-05-28 00:11:51','arduino'),(63,2,'Toggle Relay','1','2021-05-28 00:13:03','arduino'),(64,2,'Toggle Relay','1','2021-05-28 00:13:11','arduino'),(65,2,'Toggle Relay','1','2021-05-28 00:15:53','arduino'),(66,2,'Toggle Relay','1','2021-05-28 00:16:21','arduino'),(67,2,'Toggle Relay','1','2021-05-28 00:16:23','arduino'),(68,2,'Toggle Relay','1','2021-05-28 00:16:25','arduino'),(69,2,'Toggle Relay','1','2021-05-28 00:17:50','arduino'),(70,2,'Toggle Relay','0','2021-05-28 00:19:01','arduino'),(71,2,'Toggle Relay','1','2021-05-28 00:19:20','arduino'),(72,2,'Toggle Relay','1','2021-05-28 00:19:22','arduino'),(73,2,'Toggle Relay','1','2021-05-28 00:19:24','raspberry'),(74,2,'Toggle Relay','0','2021-05-28 00:19:25','raspberry'),(75,2,'Toggle Relay','1','2021-05-28 00:19:26','raspberry'),(76,2,'Toggle Relay','0','2021-05-28 00:19:27','raspberry'),(77,2,'Toggle Relay','1','2021-05-28 00:19:27','arduino'),(78,2,'Toggle Relay','1','2021-05-28 00:20:59','raspberry'),(79,2,'Toggle Relay','0','2021-05-28 00:21:00','raspberry'),(80,2,'Toggle Relay','1','2021-05-28 00:21:01','arduino'),(81,2,'Toggle Relay','1','2021-05-28 00:21:03','arduino'),(82,2,'Toggle Relay','1','2021-05-28 00:21:05','arduino'),(83,2,'Toggle Relay','1','2021-05-28 00:26:25','arduino'),(84,2,'Toggle Relay','1','2021-05-28 00:28:02','arduino'),(85,2,'Toggle Relay','1','2021-05-28 00:29:19','arduino'),(86,2,'Toggle Relay','1','2021-05-28 00:31:30','raspberry'),(87,2,'Toggle Relay','0','2021-05-28 00:31:31','raspberry'),(88,2,'Toggle Relay','1','2021-05-28 00:31:32','arduino'),(89,2,'Toggle Relay','1','2021-05-28 00:35:16','arduino'),(90,2,'Toggle Relay','1','2021-05-28 00:36:59','raspberry'),(91,2,'Toggle Relay','0','2021-05-28 00:37:00','raspberry'),(92,2,'Toggle Relay','1','2021-05-28 00:37:22','arduino'),(93,2,'Toggle Relay','1','2021-05-28 00:41:16','arduino'),(94,2,'Toggle Relay','1','2021-05-28 00:45:29','arduino'),(95,2,'Toggle Relay','1','2021-05-28 01:08:43','raspberry'),(96,2,'Toggle Relay','0','2021-05-28 01:08:44','raspberry'),(97,2,'Toggle Relay','1','2021-05-28 01:13:56','arduino'),(98,2,'Toggle Relay','1','2021-05-28 01:14:40','raspberry'),(99,2,'Toggle Relay','0','2021-05-28 01:14:41','raspberry'),(100,2,'Toggle Relay','1','2021-05-28 01:14:42','arduino'),(101,2,'Toggle Relay','1','2021-05-28 01:14:43','arduino'),(102,2,'Toggle Relay','1','2021-05-28 01:14:45','arduino'),(103,2,'Toggle Relay','1','2021-05-28 01:14:47','arduino'),(104,2,'Toggle Relay','1','2021-05-28 01:14:48','arduino'),(105,2,'Toggle Relay','1','2021-05-28 01:14:54','arduino'),(106,2,'Toggle Relay','1','2021-05-28 01:19:07','arduino'),(107,2,'Toggle Relay','1','2021-05-28 01:19:09','arduino'),(108,2,'Toggle Relay','1','2021-05-28 01:21:44','arduino'),(109,2,'Toggle Relay','0','2021-05-28 01:21:47','arduino'),(110,2,'Toggle Relay','1','2021-05-28 01:21:49','arduino'),(111,2,'Toggle Relay','0','2021-05-28 01:21:51','arduino'),(112,2,'Toggle Relay','1','2021-05-28 01:21:52','arduino'),(113,2,'Toggle Relay','0','2021-05-28 01:21:54','arduino'),(114,2,'Toggle Relay','1','2021-05-28 01:21:55','raspberry'),(115,2,'Toggle Relay','0','2021-05-28 01:21:55','raspberry'),(116,2,'Toggle Relay','1','2021-05-28 01:21:56','raspberry'),(117,2,'Toggle Relay','0','2021-05-28 01:21:56','raspberry'),(118,2,'Toggle Relay','1','2021-05-28 01:21:57','arduino'),(119,2,'Toggle Relay','0','2021-05-28 01:21:59','arduino'),(120,2,'Toggle Relay','1','2021-05-28 01:22:17','arduino'),(121,2,'Toggle Relay','0','2021-05-28 01:22:19','arduino'),(122,2,'Toggle Relay','1','2021-05-28 01:22:27','arduino'),(123,2,'Toggle Relay','0','2021-05-28 01:22:29','arduino'),(124,2,'Toggle Relay','1','2021-05-28 01:22:47','arduino'),(125,2,'Toggle Relay','0','2021-05-28 01:22:48','arduino'),(126,2,'Toggle Relay','1','2021-05-28 01:23:03','arduino'),(127,2,'Toggle Relay','0','2021-05-28 01:23:04','arduino'),(128,2,'Toggle Relay','1','2021-05-28 01:24:07','arduino'),(129,2,'Toggle Relay','0','2021-05-28 01:24:08','arduino'),(130,2,'Toggle Relay','1','2021-05-28 01:24:56','arduino'),(131,2,'Toggle Relay','0','2021-05-28 01:24:58','arduino'),(132,2,'Toggle Relay','1','2021-05-28 01:25:27','arduino'),(133,2,'Toggle Relay','0','2021-05-28 01:25:28','arduino'),(134,2,'Toggle Relay','1','2021-05-28 01:26:19','arduino'),(135,2,'Toggle Relay','0','2021-05-28 01:26:21','arduino'),(136,2,'Toggle Relay','1','2021-05-28 01:27:51','arduino'),(137,2,'Toggle Relay','0','2021-05-28 01:28:43','arduino'),(138,2,'Toggle Relay','1','2021-05-28 01:28:44','raspberry'),(139,2,'Toggle Relay','0','2021-05-28 01:28:46','raspberry'),(140,2,'Toggle Relay','1','2021-05-28 02:40:58','raspberry'),(141,2,'Toggle Relay','1','2021-05-28 02:40:59','arduino'),(142,2,'Toggle Relay','0','2021-05-28 02:41:00','arduino'),(143,2,'Toggle Relay','0','2021-05-28 02:41:01','raspberry'),(144,2,'Toggle Relay','1','2021-05-28 02:45:25','arduino'),(145,2,'Toggle Relay','1','2021-05-28 02:45:27','raspberry'),(146,2,'Toggle Relay','0','2021-05-28 02:45:28','raspberry'),(147,2,'Toggle Relay','0','2021-05-28 02:45:29','arduino'),(148,2,'Toggle Relay','1','2021-05-28 02:48:39','arduino'),(149,2,'Toggle Relay','1','2021-05-28 02:48:43','raspberry'),(150,2,'Toggle Relay','0','2021-05-28 02:48:44','raspberry'),(151,2,'Toggle Relay','0','2021-05-28 02:48:46','arduino'),(152,2,'Toggle Relay','1','2021-05-28 03:58:22','arduino'),(153,2,'Toggle Relay','0','2021-05-28 03:58:28','arduino'),(154,2,'Toggle Relay','1','2021-05-28 03:58:29','raspberry'),(155,2,'Toggle Relay','0','2021-05-28 03:58:31','raspberry');
/*!40000 ALTER TABLE `relay_logs` ENABLE KEYS */;

--
-- Table structure for table `samples`
--

DROP TABLE IF EXISTS `samples`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `samples` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `relacion_molar` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `temp_ttermico` int(11) DEFAULT NULL,
  `tiempo_ttermico` int(11) DEFAULT NULL COMMENT 'Minutos',
  `tiempo_secado` int(11) DEFAULT NULL COMMENT 'Minutos',
  `tiempo_lavado` int(11) DEFAULT NULL COMMENT 'Minutos',
  `fase` int(11) DEFAULT NULL,
  `tecnicas` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`tecnicas`)),
  `observaciones` text DEFAULT NULL,
  `created_at` timestamp DEFAULT current_timestamp(),
  `hora_inicio` time DEFAULT '00:00:00',
  `hora_fin` time DEFAULT curtime(),
  `dia_inicio` date DEFAULT curdate(),
  `dia_fin` date DEFAULT curdate(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `samples_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `samples`
--

/*!40000 ALTER TABLE `samples` DISABLE KEYS */;
INSERT INTO `samples` (`id`, `name`, `relacion_molar`, `temp_ttermico`, `tiempo_ttermico`, `tiempo_secado`, `tiempo_lavado`, `fase`, `tecnicas`, `observaciones`, `created_at`, `hora_inicio`, `hora_fin`, `dia_inicio`, `dia_fin`) VALUES (1,'Muestra de prueba 1','3:2',51,61,121,11,6,'{\"MET\":true,\"UVVIS\":false}','Mis observaciones','2021-05-26 20:59:06','01:00:00','06:00:00','2021-05-27','2021-05-29'),(3,'Muestra de prueba 2','1:1',100,5,150,3,1,'{\"MET\":false,\"SEM\":false}','No tengo ninguna observación','2021-05-27 14:18:45','12:00:00',NULL,NULL,NULL);
/*!40000 ALTER TABLE `samples` ENABLE KEYS */;

--
-- Table structure for table `status_logs`
--

DROP TABLE IF EXISTS `status_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status_logs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `relay` tinyint(1) DEFAULT NULL,
  `tempC` float DEFAULT NULL,
  `tempF` float DEFAULT NULL,
  `humidity` float DEFAULT NULL,
  `microController` varchar(20) CHARACTER SET utf8 NOT NULL,
  `timestamp` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `status_logs_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=252 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status_logs`
--

/*!40000 ALTER TABLE `status_logs` DISABLE KEYS */;
INSERT INTO `status_logs` (`id`, `relay`, `tempC`, `tempF`, `humidity`, `microController`, `timestamp`) VALUES (1,0,31,87.8,25,'Raspberry_1','2021-05-27 23:58:52'),(2,0,31.3,88.3,NULL,'ESP32_1','2021-05-27 23:58:52'),(3,0,31,87.8,25,'Raspberry_1','2021-05-27 23:58:52'),(4,0,31,87.8,25,'Raspberry_1','2021-05-27 23:58:52'),(5,0,31,87.8,25,'Raspberry_1','2021-05-27 23:58:52'),(6,0,31,87.8,25,'Raspberry_1','2021-05-27 23:58:52'),(7,0,31,87.8,25,'Raspberry_1','2021-05-27 23:58:52'),(8,0,31,87.8,25,'Raspberry_1','2021-05-27 23:58:52'),(9,0,31.3,88.3,NULL,'ESP32_1','2021-05-27 23:58:52'),(10,0,31.3,88.3,NULL,'ESP32_1','2021-05-27 23:58:52'),(11,0,31.3,88.3,NULL,'ESP32_1','2021-05-27 23:58:52'),(12,0,31.3,88.3,NULL,'ESP32_1','2021-05-27 23:58:52'),(13,0,31.3,88.4,NULL,'ESP32_1','2021-05-27 23:58:52'),(14,0,31,87.8,25,'Raspberry_1','2021-05-27 23:59:05'),(15,0,30,86,25,'Raspberry_1','2021-05-27 23:59:10'),(16,0,31,87.8,25,'Raspberry_1','2021-05-27 23:59:15'),(17,0,30,86,25,'Raspberry_1','2021-05-27 23:59:20'),(18,0,30,86,25,'Raspberry_1','2021-05-27 23:59:25'),(19,0,31,87.8,26,'Raspberry_1','2021-05-27 23:59:30'),(20,0,31,87.8,27,'Raspberry_1','2021-05-27 23:59:35'),(21,0,31.3,88.4,NULL,'ESP32_1','2021-05-27 23:59:40'),(22,0,31,87.8,26,'Raspberry_1','2021-05-27 23:59:40'),(23,0,31.3,88.3,NULL,'ESP32_1','2021-05-27 23:59:45'),(24,0,31,87.8,26,'Raspberry_1','2021-05-27 23:59:45'),(25,0,31.3,88.3,NULL,'ESP32_1','2021-05-27 23:59:50'),(26,0,31,87.8,25,'Raspberry_1','2021-05-27 23:59:50'),(27,0,31.3,88.3,NULL,'ESP32_1','2021-05-27 23:59:55'),(28,0,31,87.8,25,'Raspberry_1','2021-05-27 23:59:55'),(29,0,31.3,88.3,NULL,'ESP32_1','2021-05-28 00:00:00'),(30,0,31,87.8,25,'Raspberry_1','2021-05-28 00:00:00'),(31,0,31.3,88.3,NULL,'ESP32_1','2021-05-28 00:00:05'),(32,0,30,86,25,'Raspberry_1','2021-05-28 00:00:05'),(33,0,31.3,88.3,NULL,'ESP32_1','2021-05-28 00:00:10'),(34,0,31,87.8,26,'Raspberry_1','2021-05-28 00:00:10'),(35,0,31.3,88.3,NULL,'ESP32_1','2021-05-28 00:00:15'),(36,0,30,86,26,'Raspberry_1','2021-05-28 00:00:15'),(37,0,31,87.8,26,'Raspberry_1','2021-05-28 00:01:43'),(38,0,31.3,88.3,NULL,'ESP32_1','2021-05-28 00:01:44'),(39,1,31.3,88.3,NULL,'ESP32_1','2021-05-28 00:02:50'),(40,0,31.3,88.3,NULL,'ESP32_1','2021-05-28 00:02:51'),(41,1,30,86,26,'Raspberry_1','2021-05-28 00:02:52'),(42,0,30,86,26,'Raspberry_1','2021-05-28 00:02:53'),(43,0,31.3,88.3,NULL,'ESP32_1','2021-05-28 00:07:48'),(44,0,31.3,88.3,NULL,'ESP32_1','2021-05-28 00:07:54'),(45,0,31.3,88.4,NULL,'ESP32_1','2021-05-28 00:08:09'),(46,0,31.3,88.4,NULL,'ESP32_1','2021-05-28 00:08:14'),(47,0,31.3,88.3,NULL,'ESP32_1','2021-05-28 00:08:19'),(48,0,31.3,88.4,NULL,'ESP32_1','2021-05-28 00:08:24'),(49,0,31.3,88.4,NULL,'ESP32_1','2021-05-28 00:08:29'),(50,0,31.3,88.4,NULL,'ESP32_1','2021-05-28 00:08:34'),(51,0,31.3,88.4,NULL,'ESP32_1','2021-05-28 00:08:39'),(52,0,31.3,88.4,NULL,'ESP32_1','2021-05-28 00:08:44'),(53,0,31.3,88.4,NULL,'ESP32_1','2021-05-28 00:08:45'),(54,0,31.3,88.4,NULL,'ESP32_1','2021-05-28 00:08:49'),(55,0,31.3,88.4,NULL,'ESP32_1','2021-05-28 00:08:55'),(56,0,31.3,88.4,NULL,'ESP32_1','2021-05-28 00:08:59'),(57,0,31.3,88.4,NULL,'ESP32_1','2021-05-28 00:09:04'),(58,0,31.3,88.4,NULL,'ESP32_1','2021-05-28 00:09:12'),(59,0,31.3,88.4,NULL,'ESP32_1','2021-05-28 00:09:16'),(60,0,31.3,88.4,NULL,'ESP32_1','2021-05-28 00:09:17'),(61,0,31.3,88.3,NULL,'ESP32_1','2021-05-28 00:09:47'),(62,0,31.3,88.3,NULL,'ESP32_1','2021-05-28 00:10:19'),(63,0,31.3,88.3,NULL,'ESP32_1','2021-05-28 00:10:33'),(64,0,31.3,88.3,NULL,'ESP32_1','2021-05-28 00:10:33'),(65,0,30,86,24,'Raspberry_1','2021-05-28 00:11:12'),(66,0,30,86,23,'Raspberry_1','2021-05-28 00:11:18'),(67,0,31,87.8,24,'Raspberry_1','2021-05-28 00:11:21'),(68,0,31,87.8,24,'Raspberry_1','2021-05-28 00:11:22'),(69,0,31,87.8,24,'Raspberry_1','2021-05-28 00:11:22'),(70,0,31,87.8,24,'Raspberry_1','2021-05-28 00:11:22'),(71,0,31.3,88.4,NULL,'ESP32_1','2021-05-28 00:11:47'),(72,0,31.3,88.3,NULL,'ESP32_1','2021-05-28 00:11:52'),(73,0,31.3,88.3,NULL,'ESP32_1','2021-05-28 00:12:25'),(74,0,31.2,88.1,NULL,'ESP32_1','2021-05-28 00:12:47'),(75,0,30,86,23,'Raspberry_1','2021-05-28 00:13:00'),(76,0,31.3,88.3,NULL,'ESP32_1','2021-05-28 00:13:01'),(77,0,31.3,88.3,NULL,'ESP32_1','2021-05-28 00:13:04'),(78,0,30,86,23,'Raspberry_1','2021-05-28 00:13:09'),(79,0,31.3,88.3,NULL,'ESP32_1','2021-05-28 00:13:10'),(80,0,31.3,88.3,NULL,'ESP32_1','2021-05-28 00:13:12'),(81,0,30,86,23,'Raspberry_1','2021-05-28 00:15:49'),(82,0,31.3,88.3,NULL,'ESP32_1','2021-05-28 00:15:50'),(83,0,31.3,88.3,NULL,'ESP32_1','2021-05-28 00:15:54'),(84,0,31.3,88.3,NULL,'ESP32_1','2021-05-28 00:16:19'),(85,0,30,86,23,'Raspberry_1','2021-05-28 00:16:20'),(86,0,31.3,88.3,NULL,'ESP32_1','2021-05-28 00:16:22'),(87,0,31.3,88.3,NULL,'ESP32_1','2021-05-28 00:16:23'),(88,0,31.3,88.3,NULL,'ESP32_1','2021-05-28 00:16:26'),(89,0,30,86,23,'Raspberry_1','2021-05-28 00:17:31'),(90,0,31.3,88.4,NULL,'ESP32_1','2021-05-28 00:17:32'),(91,0,30,86,24,'Raspberry_1','2021-05-28 00:17:45'),(92,0,31.3,88.4,NULL,'ESP32_1','2021-05-28 00:17:46'),(93,0,31.3,88.4,NULL,'ESP32_1','2021-05-28 00:17:51'),(94,0,31.3,88.3,NULL,'ESP32_1','2021-05-28 00:19:02'),(95,0,30,86,24,'Raspberry_1','2021-05-28 00:19:17'),(96,0,31.3,88.3,NULL,'ESP32_1','2021-05-28 00:19:18'),(97,0,31.3,88.4,NULL,'ESP32_1','2021-05-28 00:19:20'),(98,0,31.3,88.4,NULL,'ESP32_1','2021-05-28 00:19:23'),(99,1,30,86,22,'Raspberry_1','2021-05-28 00:19:24'),(100,0,30,86,22,'Raspberry_1','2021-05-28 00:19:25'),(101,1,30,86,22,'Raspberry_1','2021-05-28 00:19:26'),(102,0,30,86,22,'Raspberry_1','2021-05-28 00:19:27'),(103,0,31.3,88.4,NULL,'ESP32_1','2021-05-28 00:19:28'),(104,0,30,86,23,'Raspberry_1','2021-05-28 00:20:55'),(105,0,31.3,88.3,NULL,'ESP32_1','2021-05-28 00:20:56'),(106,1,30,86,22,'Raspberry_1','2021-05-28 00:20:59'),(107,0,30,86,22,'Raspberry_1','2021-05-28 00:21:00'),(108,0,31.3,88.3,NULL,'ESP32_1','2021-05-28 00:21:02'),(109,0,31.3,88.3,NULL,'ESP32_1','2021-05-28 00:21:04'),(110,0,31.2,88.1,NULL,'ESP32_1','2021-05-28 00:21:06'),(111,0,31,87.8,23,'Raspberry_1','2021-05-28 00:26:07'),(112,0,31.2,88.1,NULL,'ESP32_1','2021-05-28 00:26:18'),(113,0,31.2,88.1,NULL,'ESP32_1','2021-05-28 00:27:55'),(114,0,31.2,88.1,NULL,'ESP32_1','2021-05-28 00:29:15'),(115,0,31.1,88,NULL,'ESP32_1','2021-05-28 00:31:16'),(116,1,30,86,23,'Raspberry_1','2021-05-28 00:31:30'),(117,0,30,86,23,'Raspberry_1','2021-05-28 00:31:31'),(118,0,31.2,88.1,NULL,'ESP32_1','2021-05-28 00:35:07'),(119,0,31,87.8,23,'Raspberry_1','2021-05-28 00:35:13'),(120,0,31.2,88.1,NULL,'ESP32_1','2021-05-28 00:35:15'),(121,0,31,87.8,24,'Raspberry_1','2021-05-28 00:36:30'),(122,0,31,87.8,23,'Raspberry_1','2021-05-28 00:36:53'),(123,1,31,87.8,24,'Raspberry_1','2021-05-28 00:36:59'),(124,0,31,87.8,24,'Raspberry_1','2021-05-28 00:37:00'),(125,0,31,87.8,23,'Raspberry_1','2021-05-28 00:37:19'),(126,0,31,87.8,23,'Raspberry_1','2021-05-28 00:37:20'),(127,0,31.2,88.1,NULL,'ESP32_1','2021-05-28 00:37:21'),(128,0,31.2,88.1,NULL,'ESP32_1','2021-05-28 00:37:35'),(129,0,31.2,88.1,NULL,'ESP32_1','2021-05-28 00:41:30'),(130,0,31,87.8,23,'Raspberry_1','2021-05-28 00:44:40'),(131,0,31.2,88.1,NULL,'ESP32_1','2021-05-28 00:45:25'),(132,0,30,86,22,'Raspberry_1','2021-05-28 01:08:41'),(133,1,30,86,22,'Raspberry_1','2021-05-28 01:08:43'),(134,0,30,86,22,'Raspberry_1','2021-05-28 01:08:44'),(135,0,30,86,23,'Raspberry_1','2021-05-28 01:10:53'),(136,0,30,86,18,'Raspberry_1','2021-05-28 01:11:10'),(137,0,30.9,87.6,NULL,'ESP32_1','2021-05-28 01:12:41'),(138,0,30,86,18,'Raspberry_1','2021-05-28 01:14:35'),(139,0,30.9,87.6,NULL,'ESP32_1','2021-05-28 01:14:38'),(140,1,30,86,24,'Raspberry_1','2021-05-28 01:14:40'),(141,0,30,86,24,'Raspberry_1','2021-05-28 01:14:41'),(142,0,31.2,88.1,NULL,'ESP32_1','2021-05-28 01:17:48'),(143,0,30,86,23,'Raspberry_1','2021-05-28 01:21:27'),(144,0,30,86,22,'Raspberry_1','2021-05-28 01:21:31'),(145,0,31.3,88.4,NULL,'ESP32_1','2021-05-28 01:21:38'),(146,1,31.4,88.5,NULL,'ESP32_1','2021-05-28 01:21:46'),(147,0,31.4,88.5,NULL,'ESP32_1','2021-05-28 01:21:48'),(148,1,31.4,88.6,NULL,'ESP32_1','2021-05-28 01:21:50'),(149,0,31.4,88.5,NULL,'ESP32_1','2021-05-28 01:21:52'),(150,1,31.4,88.6,NULL,'ESP32_1','2021-05-28 01:21:53'),(151,0,31.4,88.6,NULL,'ESP32_1','2021-05-28 01:21:54'),(152,1,30,86,22,'Raspberry_1','2021-05-28 01:21:55'),(153,0,30,86,22,'Raspberry_1','2021-05-28 01:21:55'),(154,1,30,86,22,'Raspberry_1','2021-05-28 01:21:56'),(155,0,30,86,22,'Raspberry_1','2021-05-28 01:21:56'),(156,1,31.4,88.5,NULL,'ESP32_1','2021-05-28 01:21:59'),(157,0,31.4,88.6,NULL,'ESP32_1','2021-05-28 01:22:00'),(158,0,30,86,22,'Raspberry_1','2021-05-28 01:22:15'),(159,0,31.4,88.5,NULL,'ESP32_1','2021-05-28 01:22:16'),(160,1,31.4,88.5,NULL,'ESP32_1','2021-05-28 01:22:18'),(161,0,31.4,88.5,NULL,'ESP32_1','2021-05-28 01:22:20'),(162,1,31.4,88.6,NULL,'ESP32_1','2021-05-28 01:22:28'),(163,0,31.4,88.5,NULL,'ESP32_1','2021-05-28 01:22:30'),(164,0,30,86,22,'Raspberry_1','2021-05-28 01:22:44'),(165,0,31.4,88.5,NULL,'ESP32_1','2021-05-28 01:22:45'),(166,1,31.4,88.5,NULL,'ESP32_1','2021-05-28 01:22:48'),(167,0,31.4,88.5,NULL,'ESP32_1','2021-05-28 01:22:48'),(168,0,30,86,22,'Raspberry_1','2021-05-28 01:23:01'),(169,0,31.4,88.5,NULL,'ESP32_1','2021-05-28 01:23:02'),(170,1,31.4,88.5,NULL,'ESP32_1','2021-05-28 01:23:04'),(171,0,31.4,88.5,NULL,'ESP32_1','2021-05-28 01:23:05'),(172,0,30,86,22,'Raspberry_1','2021-05-28 01:24:04'),(173,0,31.4,88.6,NULL,'ESP32_1','2021-05-28 01:24:05'),(174,1,31.4,88.6,NULL,'ESP32_1','2021-05-28 01:24:08'),(175,0,31.4,88.6,NULL,'ESP32_1','2021-05-28 01:24:09'),(176,0,30,86,23,'Raspberry_1','2021-05-28 01:24:31'),(177,0,31.5,88.7,NULL,'ESP32_1','2021-05-28 01:24:32'),(178,0,30,86,23,'Raspberry_1','2021-05-28 01:24:52'),(179,0,31.5,88.7,NULL,'ESP32_1','2021-05-28 01:24:53'),(180,1,31.5,88.7,NULL,'ESP32_1','2021-05-28 01:24:57'),(181,0,31.5,88.7,NULL,'ESP32_1','2021-05-28 01:24:59'),(182,0,30,86,23,'Raspberry_1','2021-05-28 01:25:24'),(183,0,31.6,88.8,NULL,'ESP32_1','2021-05-28 01:25:25'),(184,1,31.6,88.8,NULL,'ESP32_1','2021-05-28 01:25:27'),(185,0,31.6,88.8,NULL,'ESP32_1','2021-05-28 01:25:29'),(186,0,30,86,23,'Raspberry_1','2021-05-28 01:26:08'),(187,0,31.6,88.8,NULL,'ESP32_1','2021-05-28 01:26:12'),(188,0,30,86,24,'Raspberry_1','2021-05-28 01:26:14'),(189,0,31.6,88.8,NULL,'ESP32_1','2021-05-28 01:26:15'),(190,1,31.6,88.8,NULL,'ESP32_1','2021-05-28 01:26:20'),(191,0,31.6,88.8,NULL,'ESP32_1','2021-05-28 01:26:22'),(192,0,30,86,24,'Raspberry_1','2021-05-28 01:26:38'),(193,0,31.6,88.8,NULL,'ESP32_1','2021-05-28 01:26:39'),(194,0,30,86,24,'Raspberry_1','2021-05-28 01:26:50'),(195,0,31.6,88.8,NULL,'ESP32_1','2021-05-28 01:26:50'),(196,0,31.6,88.8,NULL,'ESP32_1','2021-05-28 01:27:08'),(197,0,30,86,24,'Raspberry_1','2021-05-28 01:27:09'),(198,0,30,86,20,'Raspberry_1','2021-05-28 01:27:36'),(199,0,31.6,88.9,NULL,'ESP32_1','2021-05-28 01:27:38'),(200,0,30,86,24,'Raspberry_1','2021-05-28 01:27:45'),(201,0,31.6,88.9,NULL,'ESP32_1','2021-05-28 01:27:46'),(202,0,30,86,24,'Raspberry_1','2021-05-28 01:27:49'),(203,0,31.6,88.9,NULL,'ESP32_1','2021-05-28 01:27:50'),(204,1,31.6,88.9,NULL,'ESP32_1','2021-05-28 01:27:54'),(205,0,31.6,88.9,NULL,'ESP32_1','2021-05-28 01:28:43'),(206,1,30,86,24,'Raspberry_1','2021-05-28 01:28:44'),(207,0,30,86,24,'Raspberry_1','2021-05-28 01:28:46'),(208,0,31,87.8,28,'Raspberry_1','2021-05-28 02:16:25'),(209,0,31.9,89.4,NULL,'ESP32_1','2021-05-28 02:16:26'),(210,0,31,87.8,23,'Raspberry_1','2021-05-28 02:17:36'),(211,0,31.9,89.5,NULL,'ESP32_1','2021-05-28 02:17:39'),(212,0,31,87.8,29,'Raspberry_1','2021-05-28 02:18:11'),(213,0,31.9,89.5,NULL,'ESP32_1','2021-05-28 02:18:13'),(214,0,31,87.8,24,'Raspberry_1','2021-05-28 02:18:39'),(215,0,31.9,89.5,NULL,'ESP32_1','2021-05-28 02:18:42'),(216,0,31,87.8,28,'Raspberry_1','2021-05-28 02:21:23'),(217,0,31.9,89.5,NULL,'ESP32_1','2021-05-28 02:21:24'),(218,0,31,87.8,29,'Raspberry_1','2021-05-28 02:26:37'),(219,0,31.9,89.5,NULL,'ESP32_1','2021-05-28 02:26:39'),(220,0,31,87.8,24,'Raspberry_1','2021-05-28 02:26:53'),(221,0,31.9,89.5,NULL,'ESP32_1','2021-05-28 02:26:55'),(222,0,31,87.8,28,'Raspberry_1','2021-05-28 02:28:19'),(223,0,31.9,89.5,NULL,'ESP32_1','2021-05-28 02:28:22'),(224,0,31,87.8,29,'Raspberry_1','2021-05-28 02:28:34'),(225,0,32,89.6,NULL,'ESP32_1','2021-05-28 02:28:36'),(226,0,31,87.8,25,'Raspberry_1','2021-05-28 02:30:27'),(227,0,32,89.6,NULL,'ESP32_1','2021-05-28 02:30:29'),(228,0,31,87.8,29,'Raspberry_1','2021-05-28 02:31:22'),(229,0,31.9,89.5,NULL,'ESP32_1','2021-05-28 02:31:25'),(230,0,31,87.8,30,'Raspberry_1','2021-05-28 02:40:51'),(231,0,31.9,89.5,NULL,'ESP32_1','2021-05-28 02:40:52'),(232,1,31,87.8,29,'Raspberry_1','2021-05-28 02:40:58'),(233,1,31.9,89.5,NULL,'ESP32_1','2021-05-28 02:41:00'),(234,0,31.9,89.5,NULL,'ESP32_1','2021-05-28 02:41:01'),(235,0,31,87.8,29,'Raspberry_1','2021-05-28 02:41:01'),(236,1,32,89.6,NULL,'ESP32_1','2021-05-28 02:45:26'),(237,1,31,87.8,29,'Raspberry_1','2021-05-28 02:45:27'),(238,0,31,87.8,29,'Raspberry_1','2021-05-28 02:45:28'),(239,0,32,89.6,NULL,'ESP32_1','2021-05-28 02:45:29'),(240,1,31,87.8,31,'Raspberry_1','2021-05-28 02:48:43'),(241,0,31,87.8,31,'Raspberry_1','2021-05-28 02:48:44'),(242,1,32,89.6,NULL,'ESP32_1','2021-05-28 02:48:44'),(243,0,32,89.6,NULL,'ESP32_1','2021-05-28 02:48:47'),(244,0,31.6,88.8,NULL,'ESP32_1','2021-05-28 03:37:33'),(245,0,31.3,88.4,NULL,'ESP32_1','2021-05-28 03:58:24'),(246,1,31.3,88.3,NULL,'ESP32_1','2021-05-28 03:58:25'),(247,0,31.3,88.4,NULL,'ESP32_1','2021-05-28 03:58:29'),(248,1,31,87.8,31,'Raspberry_1','2021-05-28 03:58:30'),(249,0,31,87.8,31,'Raspberry_1','2021-05-28 03:58:31'),(250,0,30,86,29,'Raspberry_1','2021-05-28 03:59:26'),(251,0,31.2,88.1,NULL,'ESP32_1','2021-05-28 03:59:27');
/*!40000 ALTER TABLE `status_logs` ENABLE KEYS */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `is_admin` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_user_uindex` (`user`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `user`, `password`, `is_admin`) VALUES (2,'admin','$2b$10$lIzskWXsc6fT1yMCPTDboObr31plZ2GBHaJLN/UnMNaBwf25TW7my',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-29 20:20:49
