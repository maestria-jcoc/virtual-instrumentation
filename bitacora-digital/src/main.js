import Vue from 'vue'
import VueUUID from "vue-uuid"
import Axios from 'axios'
import Swal from 'sweetalert'

import App from './App'

import vuetify from './plugins/vuetify'
import moment from './plugins/moment'
import router from './router'

Vue.config.productionTip = false

Vue.use(VueUUID)

Vue.prototype.$swal = Swal
Vue.prototype.$axios = Axios;
Vue.prototype.$axios.defaults.baseURL = process.env.VUE_APP_API_BASE_URL

new Vue({
    router,
    vuetify,
    moment,
    render: h => h(App)
}).$mount('#app')
