import Vue from 'vue'
import VueMoment from "vue-moment";

const moment = require('moment')
require('moment/locale/es-mx')

Vue.use(VueMoment, {moment})

export default moment